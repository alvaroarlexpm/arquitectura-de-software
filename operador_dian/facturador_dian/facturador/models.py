from django.db import models

# Create your models here.
from __future__ import unicode_literals

#MODELOS PARA LA EMPRESA OPERADORA DE SERVICIO DE FACTURACION ELECTRONICA

#Empresas que estan registradas como clientes de la plataforma de facturación electrónica
class empresas(models.Model):
    id_empresa = models.AutoField(primary_key=True)
    nit_empresa = models.IntegerField()
    dv_empresa = models.IntegerField()
    estado = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'empresas'

#cada uno de los tipos de usuarios actores del sistema
# valores que guarda tipos_usuario 1. superadmin 2.admin de cada empresa 3.facturadores de cada empresa
class tipos_usuario(models.Model):
    id_tipo_usuario = models.AutoField(primary_key=True)
    tipo_usuario = models.CharField(max_length=20)
    class Meta:
        managed = False
        db_table = 'tipos_usuario'


#cada uno de los usuarios que acceden al sistema Superadmin, admin de las empresas y facturadores
class usuarios(models.Model):
    id_usuario = models.AutoField(primary_key=True)
    identificacion = models.CharField(max_length=20)
    id_empresa = models.ForeignKey('empresas', db_column='id_empresa')
    id_tipo_usuario = models.ForeignKey('tipos_usuario', db_column='id_tipo_usuario')
    class Meta:
        managed = False
        db_table = 'usuarios'

#tabla de tipos de iva que puede tener un producto
# valores que guarda tipos iva 1. 0 (exento) 2. 5%  3. 10%  4. 19%
class tipos_iva(models.Model):
    id_tipo_iva = models.AutoField(primary_key=True)
    procentaje = models.FloatField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'tipos_iva'


#cada uno de los productos que facturan las empresas que utilizan la plataforma
#los productos se cargan por empresa
class productos(models.Model):
    id_producto = models.AutoField(primary_key=True)
    referencia = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=20)
    valor_unitario = models.FloatField(blank=True, null=True)
    id_tipo_iva = models.ForeignKey('tipos_iva', db_column='id_tipo_iva')
    id_empresa = models.ForeignKey('empresas', db_column='id_empresa')

    class Meta:
        managed = False
        db_table = 'productos'

#los documentos soporte de la venta de cada empresa cuando factura
class facturas(models.Model):
    id_factura = models.AutoField(primary_key=True)
    prefijo_factura = models.CharField(max_length=20)
    numero_factura =  models.IntegerField()
    id_empresa = models.ForeignKey('empresas', db_column='id_empresa')
    fecha_factura = models.DateField()
    valor_factura = models.FloatField(blank=True, null=True)
    valor_iva = models.FloatField(blank=True, null=True)
    nit_receptor = models.CharField(max_length=20)
    estado_factura = models.ForeignKey('estados_factura', db_column='id_estado')
    class Meta:
        managed = False
        db_table = 'facturas'


#cada uno de los productos que contiene una factura
class detalles_factura(models.Model):
    id_detalle_factura = models.AutoField(primary_key=True)
    id_factura = models.ForeignKey('facturas', db_column='id_factura')
    id_producto = models.ForeignKey('productos', db_column='id_producto')
    cantidad = models.IntegerField()
    valor_unitario = models.FloatField(blank=True, null=True)
    porcentaje_iva = models.FloatField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'detalles_factura'




#MODELOS PARA LA DIAN

class tipos_contribuyente(models.Model):
    id_tipo_contribuyente = models.AutoField(primary_key=True)
    tipo_contribuyente = models.CharField(max_length=20)
    class Meta:
        managed = False
        db_table = 'tipos_contribuyente'

#los contribuyentes son todos los que están registrados en la DIAN y tienen NIT o RUT
class contribuyentes(models.Model):
    nit = models.IntegerField(primary_key=True)
    dv = models.IntegerField()
    nombre_razon_social = models.CharField(max_length=60)
    direccion = models.CharField(max_length=50)
    telefono = models.CharField(max_length=20)
    email = models.CharField(max_length=20)
    tipo_contribuyente = models.ForeignKey('tipos_contribuyente', db_column='id_tipo_contribuyente')

    estado = models.NullBooleanField()
    class Meta:
        managed = False
        db_table = 'contribuyentes'

class aprobaciones(models.Model):
    id_aprobacion = models.AutoField(primary_key=True)
    nit = models.ForeignKey('contribuyentes', db_column='nit')
    nombre_razon_social = models.CharField(max_length=30)
    fecha_solicitud = models.DateField()
    fecha_aprobacion = models.DateField()
    fecha_vencimiento = models.DateField()
    prefijo = models.CharField(max_length=10)
    numero_inicial = models.IntegerField()
    numero_final = models.IntegerField()
    class Meta:
        managed = False
        db_table = 'aprobaciones'


#El estado que tiene la factura ante la DIAN
#Tiene los valores 1. En proceso 2. Aprobada 3. Negada
class estados(models.Model):
    id_estado = models.AutoField(primary_key=True)
    estado = models.CharField(max_length=20)
    class Meta:
        managed = False
        db_table = 'estados'

#Documento registrado por las empresas que facturan para ser procesado por la DIAN y se puede visualizar por
#todos los usuarios que deseen verificar (facturadores y clientes)
class facturas(models.Model):
    prefijo = models.CharField(max_length=10)
    numero_factura = models.IntegerField()
    numero_completo = models.CharField(max_length=20)
    nit_emisor = models.ForeignKey('contribuyentes', db_column='nit')
    nit_receptor = models.CharField(max_length=30)
    fecha_factura = models.DateField()
    valor_factura = models.FloatField(blank=True, null=True)
    valor_iva = models.FloatField(blank=True, null=True)
    estado = models.ForeignKey('estados', db_column='id_estado')
    class Meta:
        managed = False
        db_table = 'facturas'