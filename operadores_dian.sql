-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2019 a las 10:00:03
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `operadores_dian`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_transacciones`
--

CREATE TABLE `detalles_transacciones` (
  `id_detalle_transacciones` int(11) NOT NULL,
  `id_transaccion` int(11) NOT NULL,
  `descripcion_transaccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad_transaccion` int(11) NOT NULL,
  `valor_transaccion` double NOT NULL,
  `impuesto_transaccion` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id_empresas` int(11) NOT NULL,
  `nit_empresas` int(11) NOT NULL,
  `dv_empresas` int(11) NOT NULL,
  `nombre_empesas` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_empresas` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_empresas` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `email_empresas` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_transacciones`
--

CREATE TABLE `tipos_transacciones` (
  `id_tipos_transacciones` int(11) NOT NULL,
  `tipo_transaccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_usuario`
--

CREATE TABLE `tipos_usuario` (
  `id_tipo_usuario` int(11) NOT NULL,
  `tipo_usuario` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transacciones`
--

CREATE TABLE `transacciones` (
  `id_transaccion` int(11) NOT NULL,
  `id_empresas` int(11) NOT NULL,
  `estado_transaccion` tinyint(1) NOT NULL,
  `id_usuario_transaccion` int(11) NOT NULL,
  `fecha_transaccion` date NOT NULL,
  `id_tipo_transaccion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuarios` int(11) NOT NULL,
  `nombre_usuario` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `identificacion_usuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `id_empresas` int(11) NOT NULL,
  `id_tipos_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detalles_transacciones`
--
ALTER TABLE `detalles_transacciones`
  ADD PRIMARY KEY (`id_detalle_transacciones`),
  ADD KEY `id_transaccion` (`id_transaccion`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id_empresas`);

--
-- Indices de la tabla `tipos_transacciones`
--
ALTER TABLE `tipos_transacciones`
  ADD PRIMARY KEY (`id_tipos_transacciones`);

--
-- Indices de la tabla `tipos_usuario`
--
ALTER TABLE `tipos_usuario`
  ADD PRIMARY KEY (`id_tipo_usuario`);

--
-- Indices de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD PRIMARY KEY (`id_transaccion`),
  ADD KEY `id_empresas` (`id_empresas`),
  ADD KEY `id_tipo_transaccion` (`id_tipo_transaccion`),
  ADD KEY `id_usuario_transaccion` (`id_usuario_transaccion`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuarios`),
  ADD KEY `usuarios_ibfk_1` (`id_tipos_usuario`),
  ADD KEY `id_empresas` (`id_empresas`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `detalles_transacciones`
--
ALTER TABLE `detalles_transacciones`
  MODIFY `id_detalle_transacciones` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id_empresas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_transacciones`
--
ALTER TABLE `tipos_transacciones`
  MODIFY `id_tipos_transacciones` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipos_usuario`
--
ALTER TABLE `tipos_usuario`
  MODIFY `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `transacciones`
--
ALTER TABLE `transacciones`
  MODIFY `id_transaccion` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuarios` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detalles_transacciones`
--
ALTER TABLE `detalles_transacciones`
  ADD CONSTRAINT `detalles_transacciones_ibfk_1` FOREIGN KEY (`id_transaccion`) REFERENCES `transacciones` (`id_transaccion`);

--
-- Filtros para la tabla `transacciones`
--
ALTER TABLE `transacciones`
  ADD CONSTRAINT `transacciones_ibfk_1` FOREIGN KEY (`id_empresas`) REFERENCES `empresas` (`id_empresas`),
  ADD CONSTRAINT `transacciones_ibfk_2` FOREIGN KEY (`id_tipo_transaccion`) REFERENCES `tipos_transacciones` (`id_tipos_transacciones`),
  ADD CONSTRAINT `transacciones_ibfk_3` FOREIGN KEY (`id_usuario_transaccion`) REFERENCES `usuarios` (`id_usuarios`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipos_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_empresas`) REFERENCES `empresas` (`id_empresas`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
